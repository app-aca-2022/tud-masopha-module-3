import 'package:flutter/material.dart';

class AppTile extends StatelessWidget {
  final String appName;
  final String appImagePath;
  final String appWinningYear;

  AppTile({
    required this.appName, 
    required this.appImagePath, 
    required this.appWinningYear,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
    child: Container(
      width: 300,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.black54,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //App image
          Container(
                height: 150.0,
                width: 150.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: AssetImage(appImagePath),
                    fit: BoxFit.fill,
                  ),
                ),
              ),

          // App appName
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 8),
            child: Text(appName,
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
                ),
          ),

          // App Winning Year
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 8.0),
            child: Text(appWinningYear,
            ),
          ),     
          


        ],
      ),
    ),
    );
    
  }
}