import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:tud_masopha_module_3/screens/home_screen.dart';
import 'package:tud_masopha_module_3/screens/register_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(      
      resizeToAvoidBottomInset: false,
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: SafeArea(
        // padding: EdgeInsets.only(left: 16, top: 25, right: 16),        
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // Logo
               Container(
                height: 50.0,
                width: 50.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: AssetImage(
                      'logos/aoy.jpg'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              SizedBox(height:25),

             //Hello User
             Text('Yello!',
             style: TextStyle(
               fontSize: 34,
               fontWeight: FontWeight.bold,
              ),
             ),
      
             SizedBox(height: 10.0),
      
             Padding(
               padding: const EdgeInsets.symmetric(horizontal: 10.0),
               child: Text('Welcome back to App of The Year',
               style: TextStyle(
                 fontSize: 14,
                 color: Colors.yellow,
                ),
               ),
             ),
             
             SizedBox(height: 30.0),
      
             //Email input
             Padding(
               padding: const EdgeInsets.symmetric(horizontal: 22.0),
               child: Container(
                decoration: BoxDecoration(
                  color: Colors.grey[800],
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.circular(10),
                ),
                 child: Padding(
                   padding: const EdgeInsets.only(left: 20.0),
                   child: TextField(
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.email),
                      border: InputBorder.none,
                      hintText: 'tud@gmail.com',
                    ),
                   ),
                 ),
               ),
             ),
             SizedBox(height: 20.0), 

             //password input
             Padding(
               padding: const EdgeInsets.symmetric(horizontal: 22.0),
               child: Container(
                decoration: BoxDecoration(
                  color: Colors.grey[800],
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.circular(10),
                ),
                 child: Padding(
                   padding: const EdgeInsets.only(left: 20.0),
                   child: TextField(
                    obscureText: true,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.password),
                      border: InputBorder.none,
                      hintText: 'Password',
                    ),
                   ),
                 ),
               ),
             ),
             SizedBox(height: 20.0),
      
             //signin button
              Padding(
               padding: const EdgeInsets.symmetric(horizontal:28.0),
               child: 
               InkWell(
                onTap: navigateToHome,

                 child: Container(
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(color: Colors.yellow,
                  borderRadius: BorderRadius.circular(10),
                  ),
                  child: Center(
                    child: Text(
                      'Login',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                        color: Colors.black,
                      ),
                      ),
                  ),
                 ),
               ),
              ),
             SizedBox(height: 20.0),
      
             //register now textlink
             Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Not a Subscriber?',
                  style: TextStyle(
                   fontWeight: FontWeight.bold,
                  ),
                ),
                GestureDetector(
                  onTap: navigateToSignup,
                  child: Text(' Register Now',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.yellow,
                    ),
                  ),
                ),
              ],
             ),
      
            ],
          ),
        ),
       ),
    );    
  }

  void navigateToSignup() {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => RegisterScreen(),
        ),
    );
  }

  void navigateToHome() {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => HomeScreen(),
        ),
    );
  }
}