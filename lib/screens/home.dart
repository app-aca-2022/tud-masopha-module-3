// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:tud_masopha_module_3/utils/category_years.dart';
import 'package:tud_masopha_module_3/utils/global_variables.dart';

import 'add_screen.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int selectedIndex = 0;
  // Bottom Nav Bar onTap method
  // void bottomNavBar(index){
  //   onTap: (){

  //   }
  // }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Center(child: Text('Home')),
      ),
      
      // notched floating button
      floatingActionButton:FloatingActionButton( //Floating action button on Scaffold
         onPressed: (){
          //code to execute on button press
          Navigator.push(context,MaterialPageRoute(builder: (context) => AddApp()),);
           },
       child: Icon(Icons.add),
          backgroundColor: Colors.yellow, //icon inside button
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,

      bottomNavigationBar: BottomAppBar(
             shape: CircularNotchedRectangle(), 
             //shape of notch
             notchMargin: 8,
        child: BottomNavigationBar(
          backgroundColor: Colors.transparent,
          selectedItemColor: Colors.yellow,
          unselectedItemColor: Colors.white60,
          currentIndex: selectedIndex,
          onTap: (index) => setState(() =>  selectedIndex = index),          
            items: [
              BottomNavigationBarItem(
                icon: Icon
              (Icons.home),
              label: '',
              ),
              BottomNavigationBarItem(
                icon: Icon
                (Icons.person),
              label: '',
              ),
            ],
        ),
      ),
      
      body: HomeItems[selectedIndex]
    );
  }
}

