import 'package:flutter/material.dart';
import 'package:tud_masopha_module_3/screens/home.dart';

class AddApp extends StatefulWidget {
  AddApp({Key? key}) : super(key: key);

  @override
  State<AddApp> createState() => _AddAppState();
}

class _AddAppState extends State<AddApp> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        backgroundColor:Theme.of(context).scaffoldBackgroundColor,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: (){
            Navigator.pop(
              context, MaterialPageRoute(
                builder: (context) => Home()
                ),
                );
          },
        ),
        title: const Text('Add App'),
        centerTitle: false,
        actions: [
          TextButton(
            onPressed: (){},
             child: const Text(
              'Add',
              style: TextStyle(
                color: Colors.yellow,
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
          )
        ],
      ),
    );
  }
}